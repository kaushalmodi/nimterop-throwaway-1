import nimterop/cimport
import os

# cDisableCaching()
cDebug()

const
  # https://gitter.im/nim-lang/Nim?at=5c4e7a1754f21a71a1b8ede5
  hDir = currentSourcePath.parentDir()

cIncludeDir(hDir)
cAddSearchDir(hDir)
cImport(cSearchPath("eggx.h"))
