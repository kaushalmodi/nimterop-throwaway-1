#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

void next( FILE *fp )
{
    fpos_t pos ;
    while(1){
	fgetpos(fp,&pos) ;
	if( fgetc(fp)=='#' ){
	    while( fgetc(fp) != '\n' ) ;
	}
	else{
	    fsetpos(fp,&pos) ;
	    break ;
	}
    }
}

int numread( FILE *fp )
{
    int c,i ;
    char buf[16] ;

    while( 1 ){
	c=fgetc(fp) ;
	if( isdigit(c) ){
	    break ;
	}
    }
    i=0 ;
    buf[i]=c ;
    for( i=1 ; i<15 ; i++ ){
	c=fgetc(fp) ;
	if( isdigit(c)==0 ){
	    break ;
	}
	buf[i]=c ;
    }
    if( i==15 ) exit(-1) ;
    buf[i]='\0' ;
    return( atoi(buf) ) ;
}

int main( int argc, char *argv[] )
{
    int i,width,height,depth,ppmascii=0 ;
    FILE *fp ;
    if( argc < 2 ){
	fprintf(stderr,"Specify ppm file\n") ;
	return(0) ;
    }
    fp = fopen(argv[1],"rb") ;
    if( fp == NULL ) return(-1) ;

    next(fp) ;

    if( fgetc(fp)!='P' ) return(-1) ;
    i=fgetc(fp) ;
    if( i!='6' && i!='3' ) return(-1) ;
    if( i=='3' ) ppmascii=1 ;
    while( fgetc(fp) != '\n' ) ;

    next(fp) ;

    i=fscanf(fp,"%d %d\n",&width,&height) ;
    if( i!=2 ) return(-1) ;

    next(fp) ;

    i=fscanf(fp,"%d\n",&depth) ;
    if( i!=1 ) return(-1) ;

    printf("#define PPM_WIDTH %d\n",width) ;
    printf("#define PPM_HEIGHT %d\n",height) ;
    printf("unsigned char Ppmimage[] = { \n") ;

    if( ppmascii ){
	for( i=0 ; i < width*height*3 ; i++ ){
	    if( (i % 3)==0 ) printf("0x000,") ;
	    printf("0x%x",numread(fp)) ;
	    if( i!=(width*height*3-1) ) printf(",") ;
	    if( ((i+1) % 12)==0 ) printf("\n") ;
	}
    }
    else{
	for( i=0 ; i < width*height*3 ; i++ ){
	    if( (i % 3)==0 ) printf("0x000,") ;
	    printf("0x%x",fgetc(fp)) ;
	    if( i!=(width*height*3-1) ) printf(",") ;
	    if( ((i+1) % 12)==0 ) printf("\n") ;
	}
    }
    printf("} ;\n") ;
    fclose(fp) ;
    return(0) ;
}
