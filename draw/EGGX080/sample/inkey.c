/* egg inkey.c -o inkey -Wall */
#include <eggx.h>

#define FNTSZ 24		/* フォントサイズ */
#define FOFFSET 2
#define COL 40			/* 文字数 */
#define XSIZE FNTSZ/2*COL	/* ウィンドゥサイズ */
#define YSIZE FNTSZ*2+FOFFSET

int main()
{
    int wn,wn1 ;
    int i ;
    wn1=gopen(10*FNTSZ/2,FNTSZ+FOFFSET) ;
    wn=gopen(XSIZE,YSIZE) ;
    /* ウィンドゥの名前 */
    winname(wn1,"Key Code") ;
    winname(wn,"キーボード入力のテスト") ;
    drawstr(wn,0,FNTSZ+FOFFSET,FNTSZ,0,"[Type your keyboard!]") ; 
    for( i=0 ; i<COL ; i++ ){
	int c=ggetch(wn) ;
	if( 0x020 <= c && c <= 0x07f ){
	    char str[2] = " " ;
	    str[0] = (char)c ;
	    drawstr(wn,i*FNTSZ/2,FOFFSET,FNTSZ,0,str) ;
	}
	gclr(wn1) ;
	drawstr(wn1,0,FOFFSET,FNTSZ,0,"%3d(0x0%02x)",c,c) ;
    }
    gclose(wn1) ;
    gclose(wn) ;
    return(0) ;
}
