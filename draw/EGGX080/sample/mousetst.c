/* egg mousetst.c -o mousetst -Wall */
#include <stdio.h>
#include <eggx.h>

int main()
{
    int win,b ;
    float x=0,y=0 ;
    win = gopen(400,300) ;
    /* window(win,399,299,00,00) ; */
    /* window(win,0,299,399,00) ; */
    gsetbgcolor(win,"blue") ;
    gclr(win) ;
    
    puts("ウィンドゥ上のどこかをクリックしたり，") ;
    puts("キーボードから何か入力してみてください．") ;

    pset(win,0,0) ;
    pset(win,399,0) ;
    pset(win,0,299) ;
    pset(win,399,299) ;

    while( 1 ){
	int type ;
	type=ggetxpress(win,&b,&x,&y) ;
	if( type == ButtonPress ){
	    newpen(win,b) ;
	    line(win,0,y,PENUP) ;
	    line(win,399,y,PENDOWN) ;
	    line(win,x,0,PENUP) ;
	    line(win,x,299,PENDOWN) ;
	    drawstr(win,x,y,14,0,"%g %g",x,y) ;
	    printf("button=%d x=%g x=%g\n",b,x,y) ;
	}
	else if( type == KeyPress ){
	    if( b=='q' ) break ;
	    drawstr(win,x,y-14,14,0,"%c",b) ;
	    printf("key code = %d\n",b) ;
	    x+=7 ;
	}
    }

    gclose(win) ;
    return(0) ;
}
